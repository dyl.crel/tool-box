﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolBoxNET.Pattern.Repository
{
    public interface IDataService<TKey, TEntity>
    {
        IEnumerable<TEntity> Get();
        TEntity Get(TKey ID);
        TEntity Insert(TEntity Entity);
        bool Update(TEntity Entity);
        bool Delete(TKey ID);
    }
}
