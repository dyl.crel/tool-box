﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToolBoxNET.Pattern.Mediator
{
    public class Mediator<TMessage>
    {

        // Singleton
        private static Mediator<TMessage> _Instance;
        public static Mediator<TMessage> Instance
        {
            get { return _Instance ?? (_Instance = new Mediator<TMessage>()); }
        }

        //Delegé qui ne renvoit rien
        private Action<TMessage> _Broadcast;

        protected Mediator()
        {

        }
        public void Register(Action<TMessage> Methode)
        {
            _Broadcast += Methode;
        }
        public void UnRegister(Action<TMessage> Methode)
        {
            _Broadcast -= Methode;
        }
        
        public void Send(TMessage Message)
        {
            _Broadcast?.Invoke(Message);
        }
    }
}
