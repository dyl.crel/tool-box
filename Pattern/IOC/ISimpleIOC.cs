﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToolBoxNET.Pattern.IOC
{
    public interface ISimpleIOC
    {
        void Register<TResource>();
        void Register<TResource>(params object[] Parameters);
        void Register<TInterface, TResource>()
            where TResource : TInterface;
        void Register<TInterface, TResource>(params object[] Parameters)
            where TResource : TInterface;
        TResource GetInstance<TResource>();
    }
}
