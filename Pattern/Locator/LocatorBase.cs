﻿using System;
using System.Collections.Generic;
using System.Text;
using ToolBoxNET.Pattern.IOC;

namespace ToolBoxNET.Pattern.Locator
{
    public abstract class LocatorBase
    {
        private ISimpleIOC _Container;

        protected ISimpleIOC Container
        {
            get
            {
                return _Container;
            }

            private set
            {
                _Container = value;
            }
        }

        protected LocatorBase() : this(new SimpleIOC())
        {

        }

        protected LocatorBase(ISimpleIOC Container)
        {
            this.Container = Container;
        }
    }
}
