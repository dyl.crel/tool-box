﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace ToolBoxNET.Connection.Database
{
    public class Connection
    {
        private string _ConnectionString;
        private DbProviderFactory _Fabrique;

        public Connection(string Namespace, string ConnectionString)
        {
            _ConnectionString = ConnectionString;
            _Fabrique = DbProviderFactories.GetFactory(Namespace);

            using (DbConnection c = CreateConnection())
            {
                c.Open();
            }
        }

        public IEnumerable<TResult> ExecuteReader<TResult>(Command Command, Func<IDataRecord, TResult> Selector)
        {
            using (DbConnection c = CreateConnection())
            {
                using (DbCommand cmd = CreateCommand(Command, c))
                {
                    c.Open();
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            yield return Selector(reader);
                        }
                    }
                }
            }
        }

        public dynamic ExecuteScalar(Command Command)
        {
            using (DbConnection c = CreateConnection())
            {
                using (DbCommand cmd = CreateCommand(Command, c))
                {
                    c.Open();
                    object o = cmd.ExecuteScalar();
                    return (o is DBNull) ? null : o;
                }
            }
        }

        public int ExecuteNonQuery(Command Command)
        {
            using (DbConnection c = CreateConnection())
            {
                using (DbCommand cmd = CreateCommand(Command, c))
                {
                    c.Open();
                    return cmd.ExecuteNonQuery();
                }
            }
        }

        public DataTable GetDataTable(Command Command)
        {
            using (DbConnection c = CreateConnection())
            {
                using (DbCommand cmd = CreateCommand(Command, c))
                {
                    using (DbDataAdapter da = _Fabrique.CreateDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        return dt;
                    }
                }
            }
        }

        public DataSet GetDataSet(Command Command)
        {
            using (DbConnection c = CreateConnection())
            {
                using (DbCommand cmd = CreateCommand(Command, c))
                {
                    using (DbDataAdapter da = _Fabrique.CreateDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        return ds;
                    }
                }
            }
        }

        private DbCommand CreateCommand(Command Command, DbConnection Connection)
        {
            DbCommand cmd = Connection.CreateCommand();
            cmd.CommandText = Command.Query;
            cmd.CommandType = (Command.IsStoredProcedure) ? CommandType.StoredProcedure : CommandType.Text;

            foreach (KeyValuePair<string, object> kvp in Command.Parameters)
            {
                DbParameter Parameter = _Fabrique.CreateParameter();
                Parameter.ParameterName = kvp.Key;
                Parameter.Value = (kvp.Value == null) ? DBNull.Value : kvp.Value;

                cmd.Parameters.Add(Parameter);
            }

            return cmd;
        }

        private DbConnection CreateConnection()
        {
            DbConnection c = _Fabrique.CreateConnection();
            c.ConnectionString = _ConnectionString;

            return c;
        }
    }
}
