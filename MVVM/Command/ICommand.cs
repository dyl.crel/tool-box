﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToolBoxNET.MVVM.Command
{
    public interface ICommand : System.Windows.Input.ICommand
    {
        void RaiseCanExecuteChanged();
    }
}
