﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolBoxNET.MVVM.Command
{
    public class RelayCommandAsync : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Func<Task> _Execute;
        private readonly Predicate<object> _CanExecute;
        private bool iSExecuting;

        public RelayCommandAsync(Func<Task> Execute) : this(Execute, null)
        {

        }
        public RelayCommandAsync(Func<Task> Execute, Predicate<object> CanExecute)
        {
            if (Execute == null)
                throw new ArgumentNullException("Execute");

            _Execute = Execute;
            _CanExecute = CanExecute;
        }

        public bool CanExecute(object parameter)
        {
            return (!iSExecuting && _CanExecute == null) ? true : !iSExecuting && _CanExecute(parameter);
        }

        public async void Execute(object parameter)
        {
            iSExecuting = true;
            try
            {
            await _Execute();
            }
            finally
            {
                iSExecuting = false;
            }
        }
        public void RaiseCanExecuteChanged()
        {
           CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
