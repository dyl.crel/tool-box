﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToolBoxNET.MVVM.Command
{
    public class RelayCommandParam : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public readonly Action<object> _Execute;
        public readonly Func<bool> _CanExecute;

        public RelayCommandParam(Action<object> Execute) : this(Execute, null)
        {

        }
        public RelayCommandParam(Action<object> Execute, Func<bool> CanExecute)
        {
            if (Execute == null)
                throw new ArgumentNullException("Execute");

            _Execute = Execute;
            _CanExecute = CanExecute;
        }

        public bool CanExecute(object parameter)
        {
            return (_CanExecute == null) ? true : _CanExecute();
        }

        public void Execute(object parameter)
        {
            _Execute(parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
