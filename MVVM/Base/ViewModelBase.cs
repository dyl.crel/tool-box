﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using ToolBoxNET.MVVM.Command;

namespace ToolBoxNET.MVVM.Base
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        //Evenement pour le changement de propiété
        public event PropertyChangedEventHandler PropertyChanged;

        //Constructeur
        public ViewModelBase()
        {
            // Recuperation du type 
            Type TViewModel = GetType();

            // List enumerable de type commande
            IEnumerable<PropertyInfo> Commands = TViewModel.GetProperties().Where(PI => PI.PropertyType == typeof(ICommand) || PI.PropertyType.GetInterfaces().Contains(typeof(ICommand)));

            // Parcours la list
            foreach (PropertyInfo pi in Commands)
            {
                ICommand cmd = (ICommand)pi.GetMethod.Invoke(this, null);
                PropertyChanged += (s, e) => cmd.RaiseCanExecuteChanged();
            }
        }
        // Methode permettant de reprendre le nom de la propriété
        protected void RaisePropertyChanged([CallerMemberName]string PropertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }

    }
}
